#include <stdio.h>
#include "includes.h"
#include "altera_up_avalon_parallel_port.h"

/* Definition of Task Stacks */
#define   TASK_STACKSIZE       2048
OS_STK    task1_stk[TASK_STACKSIZE];

/* Definition of Task Priorities */

#define TASK1_PRIORITY      1
alt_up_parallel_port_dev *gpio_dev; //gpio device

/* Prints "Hello World" and sleeps for three seconds */
void task1(void* pdata)
{
	int toggle = 1;
	int gpio_values;
  while (1)
  { 
	  toggle = ! toggle;
	  if (toggle)
		  alt_up_parallel_port_write_data(gpio_dev, 0x00000002);
	  else
		  alt_up_parallel_port_write_data(gpio_dev, 0x00000009);

	  gpio_values = alt_up_parallel_port_read_data(gpio_dev);

	  gpio_values &= 0x80000000;

	  if(gpio_values == 0)
		  printf("Off\n");
	  else
		  printf("On\n");

    OSTimeDlyHMSM(0, 0, 3, 0);
  }
}
/* The main function creates two task and starts multi-tasking */
int main(void)
{

	gpio_dev = alt_up_parallel_port_open_dev("/dev/Expansion_JP1");
	alt_up_parallel_port_set_port_direction(gpio_dev, 0x0000000B);
  
  OSTaskCreateExt(task1,
                  NULL,
                  (void *)&task1_stk[TASK_STACKSIZE-1],
                  TASK1_PRIORITY,
                  TASK1_PRIORITY,
                  task1_stk,
                  TASK_STACKSIZE,
                  NULL,
                  0);
              
  OSStart();
  return 0;
}
