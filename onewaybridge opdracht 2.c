#include <stdio.h>
#include "includes.h"
#include "altera_up_avalon_parallel_port.h"
#include "altera_up_avalon_character_lcd.h"

/* Definition of Task Stacks */
#define   TASK_STACKSIZE       512
OS_STK    taskCar_stk[50][TASK_STACKSIZE];
OS_STK    taskTaskStart_stk[TASK_STACKSIZE];

/* Definition of Task Priorities */

#define TASKTaskStart_PRIORITY      9
#define MAX_CARS	50
#define GPIO_ptr ((volatile char *) 0x10000060)

volatile int currentcars = 0;

alt_up_character_lcd_dev *lcd_dev;
alt_up_parallel_port_dev *gpio_dev;

OS_EVENT *SemBridge;

/* DETECTED PROBLEMS:
 *
 * OS_EVENT*: Werkt niet op de Nios
 * ALT_SEM: Geeft errors.
 */
void taskCar(int* pdata)
{
	INT8U err;
	volatile int* red_LED_ptr = (int*) 0x10000000;
	int Car_Val;

	//	pend semaphore
	OSSemPend(SemBridge,0,&err);
	// Make the LEDs go from one side to another.
	if(pdata){
		Car_Val = 0x1;
		while(Car_Val < 0x40000){
			*(red_LED_ptr) = Car_Val;
			Car_Val = Car_Val << 1;
			OSTimeDlyHMSM(0,0,0,294);
		}
	} else {
		Car_Val = 0x20000;
		while(Car_Val > 0x0){
			*(red_LED_ptr) = Car_Val;
			Car_Val = Car_Val >> 1;
			OSTimeDlyHMSM(0,0,0,294);
		}
	}
	currentcars--;
	*(red_LED_ptr) = 0;
	err = OSSemPost(SemBridge);
	//	post semaphore

	err = OSTaskDel(OS_PRIO_SELF);
}

void taskTaskStart(void* pdata)
{
	volatile int* KEY_ptr = (int*) 0x10000050;
	volatile int* SW_switch_ptr = (int*) 0x10000040;

	int KEY_value, SW_value, Data_GPIO;
	int car_prio = 10;
	int countcar = 0;
	char buffer[8];
	int* dir = 0;
	int bits = 0x07060040;

	while(1){
		 *(GPIO_ptr+4) = 0;
		Data_GPIO = *(GPIO_ptr);

		if(Data_GPIO == 0x00000040){
			if(car_prio == MAX_CARS){
				car_prio = 10;
			}
				OSTaskCreate(taskCar,
								  dir,
								  (void *)&taskCar_stk[car_prio][TASK_STACKSIZE-1],
								  car_prio);

				car_prio++;
				countcar++;
				currentcars++;
				OSTimeDlyHMSM(0, 0, 2, 0);
		}

		//Put stuff on the display
		if(currentcars < 10){
			sprintf(buffer, "%s", " ");
			alt_up_character_lcd_set_cursor_pos(lcd_dev, 1, 1);
			alt_up_character_lcd_string(lcd_dev, buffer);
		}

		sprintf(buffer, "%d", countcar);
		alt_up_character_lcd_set_cursor_pos(lcd_dev, 13, 1);
		alt_up_character_lcd_string(lcd_dev, buffer);

		sprintf(buffer, "%d", currentcars);
		alt_up_character_lcd_set_cursor_pos(lcd_dev, 0, 1);
		alt_up_character_lcd_string(lcd_dev, buffer);

		SW_value = *(SW_switch_ptr);
		if(SW_value == 0x0){
			sprintf(buffer, "%s", "Rechtsaf");
			dir = 0;
		}else{
			sprintf(buffer, "%s", "Linksaf ");
			dir = 1;
		}
		alt_up_character_lcd_set_cursor_pos(lcd_dev, 0, 0);
		alt_up_character_lcd_string(lcd_dev, buffer);

		//check if the key is pressed and then make a car task if it is pressed.
		KEY_value = *(KEY_ptr);

		if(KEY_value != 0){
			*(GPIO_ptr+4) = 0xffffffff;
			 *(GPIO_ptr) = bits;
				while(*KEY_ptr);
			}

		OSTimeDlyHMSM(0, 0, 0, 200);
	}
}

int main(void)
{

	lcd_dev = alt_up_character_lcd_open_dev("/dev/Char_LCD_16x2");

	if(lcd_dev == NULL){
		printf("FOUT bij LCD\n");
		return -1;
	}else{
		printf("LCD is connected\n");
	}

	SemBridge = OSSemCreate(1);

	OSTaskCreate(taskTaskStart,
	                  NULL,
	                  (void *)&taskTaskStart_stk[TASK_STACKSIZE-1],
	                  TASKTaskStart_PRIORITY);


  OSStart();
  return 0;
}
